<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
// date_default_timezone_set ("Asia/Jakarta");
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

if(isset($_POST['id'])){

	$msg['message'] = '';

	$id = $_POST['id'];
	$role_id = 2;

	$select_query = "SELECT * FROM users WHERE id = '$id' AND role_id = '$role_id'";
	$select_stmt = $conn->prepare($select_query);
	$select_stmt->execute();

	if($select_stmt->rowCount() >0){

		$row = $select_stmt->fetch(PDO::FETCH_ASSOC);

		$update_query = "UPDATE users SET isactive = :isactive WHERE id = '$id' AND role_id = '$role_id'";
		$update_stmt = $conn->prepare($update_query);

		if($update_stmt->execute()){
			$msg['message'] = 'Data Updated Successfully';
	        $msg['data'] = [
	            'id' => $id,
	            'role_id' => $role_id,
	            'status_id' =>$row['status_id'],
	            'username' => $row['username'],
	            'email' => $row['email'],
	            'telephone_number' => $row['telephone_number'],
	            'no_kk' => $row['no_kk'],
	            'isactive' => $isactive
	        ];
    	}else{
    		$msg['message'] = 'Data Not Updated';
    	}
	}else{
		$msg['message'] = 'Data Not Found';
	}
}else{
	$msg['message'] = 'Please fill all the fields';
}
echo  json_encode($msg);
?>
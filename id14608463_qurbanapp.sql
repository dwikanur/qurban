-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 09, 2020 at 08:39 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id14608463_qurbanapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `photo` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `photo`) VALUES
(6, 'igiug', 'jvig', 'images/20200815203355.png'),
(7, 'RS PRATIWI', 'RSIA Praiwi melayani dengan hati, pelayanan ibu dan anak', 'images/20200815212624.png'),
(8, 'contoh', 'ini adalah artikel tentang', 'images/20200821123528.png'),
(9, 'tes', 'tes', 'images/20200821170057.png'),
(10, 'tes', 'tes', 'images/20200821170108.png'),
(11, 'jfjfifi', 'ifir', 'images/20200826064842.png'),
(12, 'contoh lg nih yaa', 'ini adalah artikel tentang apa hayoo', 'images/20200828222111.png');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `picture`, `url`) VALUES
(5, 'images/20200828083401.png', 'tes'),
(6, 'images/20200828083415.png', 'tes');

-- --------------------------------------------------------

--
-- Table structure for table `hewan_qurban`
--

CREATE TABLE `hewan_qurban` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `master_mosque_id` int(11) NOT NULL,
  `tipe_id` int(11) NOT NULL,
  `status_hewan` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `weight` varchar(255) NOT NULL,
  `photo` longtext NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hewan_qurban`
--

INSERT INTO `hewan_qurban` (`id`, `user_id`, `master_mosque_id`, `tipe_id`, `status_hewan`, `name`, `price`, `weight`, `photo`, `description`, `date`) VALUES
(15, 18, 2, 1, 0, 'Sapi quad', 'Rp. 5.000.000,-', '10 kg', 'images/20200820031326.png', 'Sehat, Muda', '2020-08-20'),
(16, 18, 2, 1, 0, 'Sapi quad', 'Rp. 5.000.000,-', '10 kg', 'images/20200820033446.png', 'Sehat, Muda', '2020-08-20'),
(17, 21, 2, 2, 0, 'Kambing', 'Rp. 3.500.000,-', '50 kg', 'images/20200821092435.png', 'Sehat, Muda', '2020-08-21'),
(18, 21, 1, 1, 0, 'dsa', '123', '123', 'images/20200826062435.png', 'dsagvjvgh', '2020-08-26'),
(19, 38, 1, 3, 0, 'ydydyfu', '6868', '58', 'images/20200826062723.png', 'hchf', '2020-08-26'),
(20, 38, 2, 1, 0, 'sapi super', 'Rp. 3.500.000,-', '50 kg', 'images/20200828221835.png', 'besar', '2020-08-28'),
(23, 38, 2, 1, 1, 'sapi josh', 'Rp 5.000.000,-', '50 kg', 'images/20200901095938.png', 'besar, josh', '2020-09-01');

-- --------------------------------------------------------

--
-- Table structure for table `master_mosque`
--

CREATE TABLE `master_mosque` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `photo` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_mosque`
--

INSERT INTO `master_mosque` (`id`, `name`, `address`, `photo`) VALUES
(1, 'Masjid An-Nur', 'Jl. Mawar No. 16 Beji Timur, Beji-Depok', 'images/20200814084113.png'),
(2, 'at taufiq', 'jalab putri tungga', 'images/20200814114006.png'),
(3, 'Musholla As Safar', 'Jl.RHM Noeradji Sumur Pacing Tangerang Karawaci', 'images/20200815142423.png'),
(4, 'Musholla As Safar', 'Jl.RHM Noeradji Sumur Pacing Tangerang Karawaci', 'images/20200815142423.png'),
(5, 'Masjid An-Nur', 'Jl. Mawar No. 16 Beji Timur, Beji-Depok', 'images/20200818165928.png');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tipe_id` int(11) NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kecamatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kelurahan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rw` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patungan_qurban` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `penagihan_patungan_qurban` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah_penagihan_qurban` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nasab` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `grup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id`, `user_id`, `tipe_id`, `alamat`, `kecamatan`, `kelurahan`, `rt`, `rw`, `telepon`, `patungan_qurban`, `penagihan_patungan_qurban`, `jumlah_penagihan_qurban`, `nasab`, `date`, `grup`) VALUES
(3, 17, 1, 'Jl. Kepundang No. 3', 'Beji', 'Beji Timur', '1', '2', '0896123487563', '5.000.000', '2 Minggu', '500.000', 'binti nicholas', '2020-08-20', 1),
(4, 19, 1, 'Jl. mawar No. 3', 'Beji', 'Beji', '1', '2', '626261655', '5.000.000', '1 Bulan', '500.000', 'bin gusana', '2020-08-20', 2),
(5, 20, 1, 'Jl. melati No. 3', 'Beji', 'Beji Timur', '1', '2', '089688887799', '5.000.000', '3 Minggu', '500.000', 'binti blackpink', '2020-08-20', 3),
(12, 20, 2, 'Jl. melati No. 3', 'Beji', 'Beji Timur', '1', '2', '089688887799', '5.000.000', '3 Minggu', '500.000', 'binti blackpink', '2020-08-20', 1),
(13, 19, 3, 'gsgsgs', 'sysysgs', 'sgsgs', '03', '03', '6272829292', '3.500.000', '1 Bulan', '400.000', 'Binti dyssyts', '2020-08-20', 4),
(14, 20, 2, 'Jl. melati No. 3', 'Beji', 'Beji Timur', '1', '2', '089688887799', '5.000.000', '3 Minggu', '500.000', 'binti blackpink', '2020-08-20', 5),
(17, 20, 1, 'jl. cinta', 'damai', 'indah', '04', '03', '089688887799', '5.000.000', '2 Minggu', '500.000', 'Binti Takeshi', '2020-08-29', 1),
(18, 40, 1, 'Jl. ninjaku', 'adalah', 'deadline', '24', 'jam', 'cuma bengong 123', '1.000.000', '1 Bulan', '50.000', 'binti market', '2020-08-29', 2),
(19, 33, 1, 'jl. madu', 'rasa', 'jeruk', '01', '01', '39388383833', '3.500.000', '1 Bulan', '100.000', 'Bin tesa', '2020-08-29', 1),
(20, 43, 1, 'Jl. belibis', 'parung', 'panjang', '24', '01', '549464646', '1.000.000', '1 Bulan', '50.000', 'ting', '2020-09-02', 1),
(21, 23, 1, 'Jl. merpati', 'terbang', 'jauh', '02', '01', '081234156438', '1.000.000', '1 Bulan', '50.000', 'wijaya', '2020-09-02', 5),
(22, 26, 1, 'hfnfj', 'vmgmg', 'mvmvmv', '03', '03', '58808555', '3.500.000', '1 Bulan', '250.000', 'Bin nvkgkg', '2020-09-02', 3),
(23, 42, 1, 'JL RHM NOERADJI GG ALIA WARGA', 'KARAWACI', 'SUMUR PACING', '01', '01', '081295828262', '3.500.000', '1 Bulan', '100.000', 'Bin SAKA BIN QURTUBI', '2020-09-02', 3),
(24, 42, 1, 'JL RHM NOERADJI GG ALIA WARGA', 'KARAWACI', 'SUMUR PACING', '01', '01', '081295828262', '3.500.000', '1 Bulan', '100.000', 'Bin SAKA BIN QURTUBI', '2020-09-02', 3);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'penjual'),
(3, 'pembeli');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status_pengambilan` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_pengambilan`) VALUES
(1, 'belum ambil daging'),
(2, 'sudah ambil daging');

-- --------------------------------------------------------

--
-- Table structure for table `tabungan`
--

CREATE TABLE `tabungan` (
  `id` int(11) NOT NULL,
  `hewan_qurban_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nominal` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tabungan_user`
--

CREATE TABLE `tabungan_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nominal` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tabungan_user`
--

INSERT INTO `tabungan_user` (`id`, `user_id`, `nominal`, `date`) VALUES
(2, 17, '50000', '2020-08-26'),
(3, 26, '10000', '2020-08-27'),
(4, 26, '88422', '2020-08-27'),
(5, 40, '50000', '2020-08-28'),
(6, 17, '50000', '2020-08-26'),
(7, 40, '20000', '2020-08-26'),
(8, 26, '10000', '2020-08-29'),
(9, 22, '50000', '2020-09-02');

-- --------------------------------------------------------

--
-- Table structure for table `tipe`
--

CREATE TABLE `tipe` (
  `id` int(11) NOT NULL,
  `tipe_hewan` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tipe`
--

INSERT INTO `tipe` (`id`, `tipe_hewan`) VALUES
(1, 'sapi'),
(2, 'kambing'),
(3, 'domba');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone_number` varchar(255) NOT NULL,
  `no_kk` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `isactive` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `status_id`, `email`, `telephone_number`, `no_kk`, `username`, `password`, `isactive`) VALUES
(17, 3, 1, 'niki@gmail.com', '0896123487563', '3311234567890', 'niki', '321', ''),
(18, 2, 1, 'penjual@gmail.com', '008', '880', 'penjual', '123', 'true'),
(19, 3, 2, 'bayu', '626261655', 'bayu', '658888555', 'anto', ''),
(20, 3, 1, 'lala@gmail.com', '089688887799', '3312223334456', 'lala', '321', ''),
(21, 2, 1, 'jual@gmail.com', '000', '3452134567123', 'jual', '123', 'true'),
(22, 3, 2, 'atung', '96794679464', '123', '09919191911', 'atung', ''),
(23, 3, 2, 'hadi@gmail.com', '081234156438', '3318889990064', 'hadi', '321', 'true'),
(24, 1, 2, 'tung', '54584787949', '123', '099929220', 'tung', ''),
(25, 3, 2, 'tungg', '25252525522', '123', '45566677', 'tungg', ''),
(26, 3, 2, 'baay', '225424512767', '123', '7262626262', '1234', 'true'),
(27, 3, 1, 'tutut@gmail.com', '089977665544', '3318765100012', 'tutut', '123', ''),
(30, 3, 1, 'ha@gmail.com', '089977665522', '3318765100011', 'hasan', '123', ''),
(31, 3, 1, 'haa@gmail.com', '089977665521', '3318765100013', 'hasa1', '123', ''),
(32, 3, 2, 'haaa@gmail.com', '089977665521', '3318765100017', 'hasaaaa', '123', ''),
(33, 3, 1, 'tesb', '39388383833', '361649494255255', 'tesb', '123', ''),
(34, 2, 1, 'bababa', '7273727272', '2627727228', 'tess', '1234', 'true'),
(35, 3, 2, 'g@gmail.com', '089977664521', 'g', 'g', '123', ''),
(36, 3, 2, 'c@gmail.com', '086977664521', 'c', 'c', '123', '3'),
(37, 2, 1, 'vugugugu', '7557575757', '88496464', 'jgiggo', 'chfufuf', 'true'),
(38, 2, 2, 'bayuu', '92837373', '346494949444', 'bayuu', '1234', 'true'),
(39, 3, 2, 'lahh', '0928320890', '34279647823678', 'lahh', 'lahh', 'true'),
(40, 3, 1, 'buyer@gmail.com', '555', 'buyer', 'buyer', '123', ''),
(41, 1, 1, 'admin', '2703333', '2703333', 'admin', '1234', ''),
(42, 3, 1, 'andyqurtubi88@gmail.com', '081295828262', '3671552323252', 'andi', '123456', 'true'),
(43, 3, 1, 'tes', '549464646', '57545484847', 'tes', 'tes', 'true'),
(44, 2, 1, 'siregar@gmail.com', '091212128723', '3671121252523', 'Diana siregar', '123456', 'false');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hewan_qurban`
--
ALTER TABLE `hewan_qurban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `master_mosque_id` (`master_mosque_id`),
  ADD KEY `FK_hewan_qurbantipe_tipe` (`tipe_id`);

--
-- Indexes for table `master_mosque`
--
ALTER TABLE `master_mosque`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_pendaftaranusers_users` (`user_id`),
  ADD KEY `FK_pendaftarantipe_tipe` (`tipe_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabungan`
--
ALTER TABLE `tabungan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hewan_qurban_id` (`hewan_qurban_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tabungan_user`
--
ALTER TABLE `tabungan_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tipe`
--
ALTER TABLE `tipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `FK_usersstatus_status` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hewan_qurban`
--
ALTER TABLE `hewan_qurban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `master_mosque`
--
ALTER TABLE `master_mosque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tabungan`
--
ALTER TABLE `tabungan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabungan_user`
--
ALTER TABLE `tabungan_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tipe`
--
ALTER TABLE `tipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hewan_qurban`
--
ALTER TABLE `hewan_qurban`
  ADD CONSTRAINT `FK_hewan_qurbanmaster_mosque_master_mosque` FOREIGN KEY (`master_mosque_id`) REFERENCES `master_mosque` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_hewan_qurbantipe_tipe` FOREIGN KEY (`tipe_id`) REFERENCES `tipe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_hewan_qurbanusers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD CONSTRAINT `FK_pendaftarantipe_tipe` FOREIGN KEY (`tipe_id`) REFERENCES `tipe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_pendaftaranusers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tabungan`
--
ALTER TABLE `tabungan`
  ADD CONSTRAINT `FK_tabunganhewan_qurban_hewan_qurban` FOREIGN KEY (`hewan_qurban_id`) REFERENCES `hewan_qurban` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_tabunganusers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tabungan_user`
--
ALTER TABLE `tabungan_user`
  ADD CONSTRAINT `FK_tabungan_userusers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_usersrole_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_usersstatus_status` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

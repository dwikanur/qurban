<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// $data = json_decode(file_get_contents("php://input"));

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
if(isset($_POST['tipe_id'])){
    $tipe_id = $_POST['tipe_id'];

    $get_pendaftaran = "SELECT p.grup, p.date,users.id,p.alamat,p.nasab,users.telephone_number,users.username,users.no_kk ,
        SUM(tabungan_user.nominal) total_tabungan,p.patungan_qurban,p.penagihan_patungan_qurban,p.jumlah_penagihan_qurban
FROM    users
        LEFT JOIN tabungan_user
            ON users.id = tabungan_user.user_id
        LEFT JOIN pendaftaran p
            ON users.id = p.user_id
            where p.tipe_id = '$tipe_id'
GROUP BY p.grup ,p.date,users.id, p.alamat,p.nasab,users.email,users.telephone_number,users.username,users.no_kk,p.patungan_qurban,p.penagihan_patungan_qurban,p.jumlah_penagihan_qurban ORDER BY p.grup ASC";
    $get_stmt = $conn->prepare($get_pendaftaran);
    $get_stmt->execute();

    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($get_stmt->rowCount() > 0){
        
        // CREATE POSTS ARRAY
    $array = [];
        
        while($row = $get_stmt->fetch(PDO::FETCH_ASSOC)){
            
            $data = [
                'id' => $row['id'],
                'username' => $row['username'],
                'grup' => $row['grup'],
                'alamat' => $row['alamat'],
                'telephone_number' => $row['telephone_number'],
                'no_kk' =>$row['no_kk'],
                'penagihan_patungan_qurban' => $row['penagihan_patungan_qurban'],
                'jumlah_penagihan_qurban' => $row['jumlah_penagihan_qurban'],
                'total_tabungan' => $row['total_tabungan'],
                'nasab' => $row['nasab'],
                'date' => $row['date']            ];
            // PUSH POST DATA IN OUR $posts_array ARRAY
            array_push($array, $data);
        }
        $response['data'] = $array;
        //SHOW POST/POSTS IN JSON FORMAT
        echo json_encode($response);

    }
    else{
        //IF THER IS NO POST IN OUR DATABASE
        echo json_encode(['message'=>'Data tidak tersedia']);
    }
}else{
    echo json_encode(['message'=>'tipe tidak tersedia']);
}
?>
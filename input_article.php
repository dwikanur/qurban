<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();
date_default_timezone_set ("Asia/Jakarta");

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg =[];

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($_POST['title']) && isset($_POST['description']) && isset($_POST['photo'])){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($_POST['title']) && !empty($_POST['photo']) && !empty($_POST['description'])){

        $title = $_POST['title'];
        $photo = $_POST['photo'];
        $fileName = date('YmdHis') . '.png';
        $filePath = 'images/' .$fileName;
        file_put_contents($filePath, base64_decode($photo));
        $description = $_POST['description'];
        
        $insert_query = "INSERT INTO articles (title,description,photo) VALUES (:title,:description,:photo)";
        
        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':title', htmlspecialchars(strip_tags($title)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':description', htmlspecialchars(strip_tags($description)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':photo', htmlspecialchars(strip_tags($filePath)),PDO::PARAM_STR);
    
        if($insert_stmt->execute()){
            $msg['message'] = 'Data Inserted Successfully';
            $msg['data'] = [
                'title' => $title,
                'description' => $description,
                'photo' => $photo
            ];
        }
        else{
            $msg['message'] = 'Data not Inserted';
        } 
        
    }else{
        $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>
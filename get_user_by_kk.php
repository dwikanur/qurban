<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// $data = json_decode(file_get_contents("php://input"));
$msg = [];

if(isset($_POST['no_kk'])){

    $no_kk = $_POST['no_kk'];

    $get_users = "SELECT users.id, users.role_id, users.status_id, users.email, users.telephone_number, users.no_kk, users.username, users.isactive
    FROM users WHERE users.no_kk = '$no_kk'";
    $get_stmt = $conn->prepare($get_users);
    $get_stmt->execute();
    $hasildata = $get_stmt->fetch();

    if($get_stmt->rowCount() > 0){
        $msg['message'] = 'Data ditemukan';
        $msg['data'] = [
            'id' => $hasildata['id'],
            'role_id' => $hasildata['role_id'],
            'status_id' =>$hasildata['status_id'],
            'email' => $hasildata['email'],
            'telephone_number' => $hasildata['telephone_number'],
            'no_kk' => $hasildata['no_kk'],
            'username' => $hasildata['username'],
            'isactive' => $hasildata['isactive']
        ];

    }else{
        $msg['message'] = 'Data tidak ditemukan';
    } 
}
else{
 $msg['message'] = 'Please fill all the fields';
}
echo  json_encode($msg);
?>
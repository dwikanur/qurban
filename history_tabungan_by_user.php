<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

if(isset($_POST['user_id'])){
    
    $user_id = $_POST['user_id'];

    // MAKE SQL QUERY
    // IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
    $query = "SELECT tabungan_user.id, tabungan_user.user_id, tabungan_user.nominal, tabungan_user.date, users.email, users.telephone_number, users.no_kk, users.username
    FROM users
    JOIN tabungan_user ON users.id = tabungan_user.user_id
    WHERE user_id = '$user_id'
    ORDER BY date ASC"; 

    $stmt = $conn->prepare($query);

    $stmt->execute();

    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($stmt->rowCount() > 0){
        
        // CREATE POSTS ARRAY
    $array = [];
        
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            
            $data = [
                'id tabungan' => $row['id'],
                'user' => [
                    'id user' => $user_id,
                    'email' => $row['email'],
                    'nomor telepon' => $row['telephone_number'],
                    'username' => $row['username'],
                    'nomor KK' => $row['no_kk']
                ],
                'nominal' => $row['nominal'],
                'date' => $row['date']
            ];
            // PUSH POST DATA IN OUR $posts_array ARRAY
            array_push($array, $data);
        }
        $response['data'] = $array;
        //SHOW POST/POSTS IN JSON FORMAT
        echo json_encode($response);

    }
    else{
        //IF THER IS NO POST IN OUR DATABASE
        echo json_encode(['message'=>'No post found']);
    }
}else{

}
?>
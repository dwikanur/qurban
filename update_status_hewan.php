<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
// date_default_timezone_set ("Asia/Jakarta");
$db_connection = new Database();
$conn = $db_connection->dbConnection();

$msg['message'] = '';

if(isset($_POST['id'])){
	$id_hewan = $_POST['id'];

	$select_query = "SELECT * FROM hewan_qurban WHERE id = '$id_hewan' AND status_hewan = '0'";
	$select_stmt = $conn->prepare($select_query);
	$select_stmt->execute();
	$hasildata = $select_stmt->fetch();

	if($select_stmt->rowCount()>0){
		$update_status = 1;

		$update_query = "UPDATE hewan_qurban SET status_hewan = '$update_status'  WHERE id = '$id_hewan'";
		$update_stmt = $conn->prepare($update_query);

		if($update_stmt->execute()){
			$msg['message'] = 'Berhasil Update';
			$msg['status'] = 'Terjual';
    	}else{
    		$msg['message'] = 'Data Not Updated';
    	}
	}else{
		$update = 0;

		$update_query = "UPDATE hewan_qurban SET status_hewan = '$update'  WHERE id = '$id_hewan'";
		$update_stmt = $conn->prepare($update_query);

		if($update_stmt->execute()){
			$msg['message'] = 'Berhasil Update';
			$msg['status'] = 'Tersedia';
    	}else{
    		$msg['message'] = 'Data Not Updated';
    	}
	}
	
}else{
	$msg['message'] = 'param tidak boleh kosong';
}
echo  json_encode($msg);
?>
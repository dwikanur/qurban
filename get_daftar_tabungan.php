<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
$query = "SELECT pendaftaran.id, pendaftaran.user_id, pendaftaran.tipe_id, pendaftaran.alamat, pendaftaran.kecamatan, pendaftaran.kelurahan, pendaftaran.rt, pendaftaran.rw, pendaftaran.telepon, pendaftaran.patungan_qurban, pendaftaran.penagihan_patungan_qurban, pendaftaran.jumlah_penagihan_qurban, pendaftaran.nasab, pendaftaran.date, users.email, users.telephone_number, users.no_kk, users.username, tipe.tipe_hewan FROM pendaftaran
JOIN tipe ON pendaftaran.tipe_id = tipe.id
JOIN users ON pendaftaran.user_id = users.id"; 

$stmt = $conn->prepare($query);

$stmt->execute();

//CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
if($stmt->rowCount() > 0){
    
    // CREATE POSTS ARRAY
$array = [];
    
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        
        $data = [
            'id pendaftaran' => $row['id'],
            'user' => [
                'id user' => $row['user_id'],
                'email' => $row['email'],
                'nomor telepon' => $row['telephone_number'],
                'username' => $row['username'],
                'nomor KK' => $row['no_kk']
            ],
            'tipe hewan qurban' => [
                'id hewan' => $row['tipe_id'],
                'tipe hewan' => $row['tipe_hewan']
            ],
            'alamat' => $row['alamat'],
            'kecamatan' => $row['kecamatan'],
            'kelurahan' => $row['kelurahan'],
            'rt' => $row['rt'],
            'rw' => $row['rw'],
            'nomor telepon' => $row['telepon'],
            'patungan_qurban' => $row['patungan_qurban'],
            'penagihan_patungan_qurban' => $row['penagihan_patungan_qurban'],
            'jumlah_penagihan_qurban' => $row['jumlah_penagihan_qurban'],
            'nasab' => $row['nasab'],
            'date' => $row['date']
        ];
        // PUSH POST DATA IN OUR $posts_array ARRAY
        array_push($array, $data);
    }
    $response['data'] = $array;
    //SHOW POST/POSTS IN JSON FORMAT
    echo json_encode($response);

}
else{
    //IF THER IS NO POST IN OUR DATABASE
    echo json_encode(['message'=>'No post found']);
}
?>
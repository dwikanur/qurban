<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// $data = json_decode(file_get_contents("php://input"));
$msg = [];

if(isset($_POST['id'])){

    $id = $_POST['id'];

    $get_users = "SELECT  users.id, users.email,users.telephone_number,users.username,users.no_kk ,
        SUM(tabungan_user.nominal) totals
FROM    users
        LEFT JOIN tabungan_user
            ON users.id = tabungan_user.user_id
where users.id = '$id'
GROUP BY users.id, users.email,users.telephone_number,users.username,users.no_kk";
    
    $get_stmt = $conn->prepare($get_users);
    $get_stmt->execute();
    $hasildata = $get_stmt->fetch();

    if($get_stmt->rowCount() > 0){
        $msg['message'] = 'Data ditemukan';
        $msg['data'] = [
            'id' => $hasildata['id'],
            'email' => $hasildata['email'],
            'totals' => $hasildata['totals'],
            'telephone_number' => $hasildata['telephone_number'],
            'no_kk' => $hasildata['no_kk'],
            'username' => $hasildata['username']
        ];

    }else{
        $msg['message'] = 'Data tidak ditemukan';
    } 
}
else{
 $msg['message'] = 'Please fill all the fields';
}
echo  json_encode($msg);
?>
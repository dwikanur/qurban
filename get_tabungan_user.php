<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// $data = json_decode(file_get_contents("php://input"));
$msg = [];

if(isset($_POST['user_id'])){

    $user_id = $_POST['user_id'];

    $get_users = "SELECT tabungan_user.id, tabungan_user.user_id, tabungan_user.nominal, tabungan_user.date, users.role_id, users.email, users.telephone_number, users.username, users.isactive FROM users
    LEFT JOIN tabungan_user ON users.id = tabungan_user.user_id
    WHERE user_id = '$user_id'";
    $get_stmt = $conn->prepare($get_users);
    $get_stmt->execute();
    $hasildata = $get_stmt->fetch();

    if($get_stmt->rowCount() > 0){
        $msg['message'] = 'Tabungan ditemukan';
        $msg['data'] = [
            'id' => $hasildata['id'],
            'user' => [
                'id' => $user_id,
                'role_id' => $hasildata['role_id'],
                'username' => $hasildata['username'],
                'email' => $hasildata['email'],
                'telephone_number' => $hasildata['telephone_number'],
                'isactive' => $hasildata['isactive']
            ],
            'nominal' => $hasildata['nominal'],
            'date' => $hasildata['date']
        ];
    }else{
        $msg['message'] = 'Belum Pernah Menabung';
        $msg['data'] = [
            'id' => $hasildata['id'],
            'user' => [
                'id' => $user_id,
                'role_id' => $hasildata['role_id'],
                'username' => $hasildata['username'],
                'email' => $hasildata['email'],
                'telephone_number' => $hasildata['telephone_number'],
                'isactive' => $hasildata['isactive']
            ],
            'nominal' => $hasildata['nominal'],
            'date' => $hasildata['date']
        ];
    } 
}
else{
 $msg['message'] = 'Please fill all the fields';
}
echo  json_encode($msg);
?>
<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

//MAKE SQL QUERY
$query_user = "SELECT  * FROM users WHERE role_id = 2";
$stmt_user = $conn->prepare($query_user);
$stmt_user->execute();

//CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
if($stmt_user->rowCount() > 0){
    
    // CREATE POSTS ARRAY
    $array = [];$array2=[];
    while($row = $stmt_user->fetch(PDO::FETCH_ASSOC)){
        $id = $row['id'];
         $msg = [
            'id' => $row['id'],
            'username' => $row['username'],
            'role_id'  => $row['role_id'],
            'email'  => $row['email'],
            'telephone_number'  => $row['telephone_number'],
            'no_kk'  => $row['no_kk'],
            'isactive' => $row['isactive']
        ];

        $query = "SELECT users.role_id, users.email, users.telephone_number, users.no_kk, users.username, users.isactive, hewan_qurban.id, hewan_qurban.name, hewan_qurban.user_id, hewan_qurban.master_mosque_id, hewan_qurban.price, hewan_qurban.weight, hewan_qurban.photo, hewan_qurban.tipe_id, hewan_qurban.description, hewan_qurban.date, hewan_qurban.status_hewan, master_mosque.name AS namess, master_mosque.address, master_mosque.photo AS photos, tipe.tipe_hewan FROM hewan_qurban
        JOIN users ON hewan_qurban.user_id = users.id
        JOIN master_mosque ON hewan_qurban.master_mosque_id = master_mosque.id
        JOIN tipe ON hewan_qurban.tipe_id = tipe.id
        JOIN role ON users.role_id = role.id
        WHERE user_id= '$id'";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            $data = [
            'hewan_qurban' => [
                'id' => $row2['id'],
                'name' => $row2['name'],
                'tipe hewan qurban' => [
                    'id hewan' => $row2['tipe_id'],
                    'tipe hewan' => $row2['tipe_hewan']
                ],
                'status_hewan' => $row2['status_hewan'],
                'master_mosque' => [
                    'id' => $row2['master_mosque_id'],
                    'name' => $row2['namess'],
                    'address' => $row2['address'],
                    'photo' => $row2['photos']
                ],
                'price' => $row2['price'],
                'weight' => $row2['weight'],
                'photo' => $row2['photo'],
                'description' => $row2['description'],
                'date' => $row2['date']
                ]
            ];
            
            // PUSH POST DATA IN OUR $posts_array ARRAY
            array_push($array2, $data);
        }
        $datas = ['penjual'=> $msg, 'hewan'=>$array2];
        array_push($array, $datas);
        $array2=[];
    }
    $response = $array;
    // $response['data penjual'] = $array;
    // $response['data'] = $array2;
    //SHOW POST/POSTS IN JSON FORMAT
    echo json_encode($response);

}
else{
    //IF THER IS NO POST IN OUR DATABASE
    echo json_encode(['message'=>'No post found']);
}
?>
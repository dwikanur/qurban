<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL P"TS
$query = "SELECT * FROM users where role_id = 2"; 

$stmt = $conn->prepare($query);

$stmt->execute();

//CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
if($stmt->rowCount() > 0){
    
    // CREATE POSTS ARRAY
$array = [];
    
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

      
        $data = [
            'id' => $row['id'],
            'username' => $row['username'],
            'email' => $row['email'],
            'no_kk' => $row['no_kk'],
            'status_id' => $row['status_id'],
            'phone' => $row['telephone_number'],
            'is_active' => $row['isactive'],
        ];
        // PUSH POST DATA IN OUR $posts_array ARRAY
        array_push($array, $data);
    }
    $response['data'] = $array;
    //SHOW POST/POSTS IN JSON FORMAT
    echo json_encode($response);

}
else{
    //IF THER IS NO POST IN OUR DATABASE
    echo json_encode(['message'=>'No post found']);
}
?>
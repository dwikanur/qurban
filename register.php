<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg =[];

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($_POST['role_id']) && isset($_POST['status_id']) && isset($_POST['email']) && isset($_POST['telephone_number']) && isset($_POST['no_kk']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['isactive'])){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($_POST['role_id']) && !empty($_POST['status_id']) && !empty($_POST['email']) && !empty($_POST['telephone_number']) && !empty($_POST['no_kk']) && !empty($_POST['username']) && !empty($_POST['password'])){

        //$pass = md5($data->password);

        $status_id = $_POST['status_id'];
        $role_id = $_POST['role_id'];
        $email = $_POST['email'];
        $username = $_POST['username'];
        $telephone_number = $_POST['telephone_number'];
        $no_kk = $_POST['no_kk'];
        $password = $_POST['password'];
        $isactive = $_POST['isactive'];

		$get_email = "SELECT users.email FROM users WHERE users.email = '$email'";
        $get_stmt = $conn->prepare($get_email);
        $get_stmt->execute();

        if($get_stmt->rowCount() >0){
        	$msg['message'] = 'Email is Already Exist';
        }else{
        	$get_username = "SELECT users.username FROM users WHERE users.username = '$username'";
        	$get_username_stmt = $conn->prepare($get_username);
        	$get_username_stmt->execute();

        	if($get_username_stmt->rowCount() >0){
        		$msg['message'] = 'Username is Already Exist';
        	}else{
        		$get_kk = "SELECT users.no_kk FROM users WHERE users.no_kk = '$no_kk'";
        		$get_kk_stmt = $conn->prepare($get_kk);
        		$get_kk_stmt->execute();

        		if($get_kk_stmt->rowCount() >0){
        			$msg['message'] = 'Nomor KK is Already Exist';
        		}else{
        			$insert_query = "INSERT INTO users (status_id,role_id,email,telephone_number,no_kk,username,password,isactive) VALUES(:status_id,:role_id,:email,:telephone_number,:no_kk,:username,:password,:isactive)";
				    $insert_stmt = $conn->prepare($insert_query);
				    // DATA BINDING
				    $insert_stmt->bindValue(':status_id', htmlspecialchars(strip_tags($status_id)),PDO::PARAM_STR);
					$insert_stmt->bindValue(':role_id', htmlspecialchars(strip_tags($role_id)),PDO::PARAM_STR);
					$insert_stmt->bindValue(':email', htmlspecialchars(strip_tags($email)),PDO::PARAM_STR);
					$insert_stmt->bindValue(':telephone_number', htmlspecialchars(strip_tags($telephone_number)),PDO::PARAM_STR);
					$insert_stmt->bindValue(':no_kk', htmlspecialchars(strip_tags($no_kk)),PDO::PARAM_STR);
					$insert_stmt->bindValue(':username', htmlspecialchars(strip_tags($username)),PDO::PARAM_STR);
					$insert_stmt->bindValue(':password', htmlspecialchars(strip_tags($password)),PDO::PARAM_STR);
					$insert_stmt->bindValue(':isactive', htmlspecialchars(strip_tags($isactive)),PDO::PARAM_STR);

					if($insert_stmt->execute()){
					    $msg['message'] = 'Data Inserted Successfully';
				        $msg['data'] = [
				           	'role_id' => $role_id,
				           	'status_id' => $status_id,
				            'email' => $email,
				            'telephone_number' => $telephone_number,
				            'nomor kartu keluarga' => $no_kk,
				            'username' => $username,
				            'isactive' => $isactive
				        ];
				    }else{
				        $msg['message'] = 'Data Not Inserted';
				    }
        		}
        	}
	    }
	}else{
		$msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>
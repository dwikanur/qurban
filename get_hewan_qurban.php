<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS

if(isset($_POST['tipe_id'])){

    $tipe_id = $_POST['tipe_id'];

    $query = "SELECT hewan_qurban.id, hewan_qurban.name, hewan_qurban.user_id, hewan_qurban.master_mosque_id, hewan_qurban.price, hewan_qurban.weight, hewan_qurban.photo, hewan_qurban.tipe_id, hewan_qurban.description, hewan_qurban.date, hewan_qurban.status_hewan, users.role_id, users.email, users.telephone_number, users.username, master_mosque.name AS namess, master_mosque.address, master_mosque.photo AS photos, tipe.tipe_hewan FROM hewan_qurban 
    JOIN users ON hewan_qurban.user_id = users.id
    JOIN master_mosque ON hewan_qurban.master_mosque_id = master_mosque.id
    JOIN tipe ON hewan_qurban.tipe_id = tipe.id
    WHERE tipe_id = '$tipe_id'
    ORDER BY date DESC"; 

    $stmt = $conn->prepare($query);

    $stmt->execute();

    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($stmt->rowCount() > 0){
        
        // CREATE POSTS ARRAY
    $array = [];

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            
            
            $photos = $row['photos'];
            $filePathMosque = 'https://qurbanapp.000webhostapp.com/' . $photos;

            $photo = $row['photo'];
            $filePathHewan = 'https://qurbanapp.000webhostapp.com/' . $photo;
            
            $data = [
                'id' => $row['id'],
                'name' => $row['name'],
                'penjual' => [
                    'id' => $row['user_id'],
                    'role_id' => $row['role_id'],
                    'username' => $row['username'],
                    'email' => $row['email'],
                    'telephone_number' => $row['telephone_number']
                ],
                'master_mosque' => [
                    'id' => $row['master_mosque_id'],
                    'name' => $row['namess'],
                    'address' => $row['address'],
                    'photo' => $filePathMosque
                ],
                'tipe hewan' => [
                    'id' => $tipe_id,
                    'tipe hewan qurban' => $row['tipe_hewan']
                ],
                'status hewan' => $row['status_hewan'],
                'price' => $row['price'],
                'weight' => $row['weight'],
                'photo' => $filePathHewan,
                'description' => $row['description'],
                'date' => $row['date']
            ];
            
            // PUSH POST DATA IN OUR $posts_array ARRAY
            array_push($array, $data);
        }
        $response['data'] = $array;
        //SHOW POST/POSTS IN JSON FORMAT
        echo json_encode($response);

    }
    else{
        //IF THER IS NO POST IN OUR DATABASE
        echo json_encode(['message'=>'No post found']);
    }
}else{

}
?>
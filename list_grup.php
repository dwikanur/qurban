<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
$query = "SELECT COUNT(*) AS jumlah, grup
FROM pendaftaran GROUP BY pendaftaran.grup
ORDER BY pendaftaran.grup DESC
LIMIT 1"; 

$stmt = $conn->prepare($query);

$stmt->execute();
$row = $stmt->fetch();

//CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
if($stmt->rowCount() > 0){
    $msg['data'] = [
        'jumlah' => $row['jumlah'],
        'grup' => $row['grup']
    ];
}
else{
    $msg['message'] = 'tidak ada grup';
}
echo  json_encode($msg);
?>
<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();
date_default_timezone_set ("Asia/Jakarta");

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg =[];

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($_POST['user_id']) && isset($_POST['tipe_id']) && isset($_POST['alamat']) && isset($_POST['kecamatan']) && isset($_POST['kelurahan']) && isset($_POST['rt']) && isset($_POST['rw']) && isset($_POST['telepon']) && isset($_POST['patungan_qurban']) && isset($_POST['penagihan_patungan_qurban']) && isset($_POST['jumlah_penagihan_qurban']) && isset($_POST['nasab']) && isset($_POST['grup'])){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($_POST['user_id']) && !empty($_POST['tipe_id']) && !empty($_POST['alamat']) && !empty($_POST['kecamatan']) && !empty($_POST['kelurahan']) && !empty($_POST['rt']) && !empty($_POST['rw']) && !empty($_POST['telepon']) && !empty($_POST['patungan_qurban']) && !empty($_POST['penagihan_patungan_qurban']) && !empty($_POST['jumlah_penagihan_qurban']) && !empty($_POST['nasab']) && !empty($_POST['grup'])){

        $user_id = $_POST['user_id'];
        $tipe_id = $_POST['tipe_id'];
        $alamat = $_POST['alamat'];
        $kecamatan = $_POST['kecamatan'];
        $kelurahan = $_POST['kelurahan'];
        $rt = $_POST['rt'];
        $rw = $_POST['rw'];
        $telepon = $_POST['telepon'];
        $patungan_qurban = $_POST['patungan_qurban'];
        $penagihan_patungan_qurban = $_POST['penagihan_patungan_qurban'];
        $jumlah_penagihan_qurban = $_POST['jumlah_penagihan_qurban'];
        $nasab = $_POST['nasab'];
        $date = date('Y-m-d');
        $grup = $_POST['grup'];

        $insert_query = "INSERT INTO pendaftaran (user_id,tipe_id,alamat,kecamatan,kelurahan,rt,rw,telepon,patungan_qurban,penagihan_patungan_qurban,jumlah_penagihan_qurban,nasab,date,grup) VALUES(:user_id,:tipe_id,:alamat,:kecamatan,:kelurahan,:rt,:rw,:telepon,:patungan_qurban,:penagihan_patungan_qurban,:jumlah_penagihan_qurban,:nasab,:date,:grup)";
        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':user_id', htmlspecialchars(strip_tags($user_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':tipe_id', htmlspecialchars(strip_tags($tipe_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':alamat', htmlspecialchars(strip_tags($alamat)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':kecamatan', htmlspecialchars(strip_tags($kecamatan)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':kelurahan', htmlspecialchars(strip_tags($kelurahan)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':rt', htmlspecialchars(strip_tags($rt)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':rw', htmlspecialchars(strip_tags($rw)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':telepon', htmlspecialchars(strip_tags($telepon)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':patungan_qurban', htmlspecialchars(strip_tags($patungan_qurban)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':penagihan_patungan_qurban', htmlspecialchars(strip_tags($penagihan_patungan_qurban)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':jumlah_penagihan_qurban', htmlspecialchars(strip_tags($jumlah_penagihan_qurban)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':nasab', htmlspecialchars(strip_tags($nasab)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':date', htmlspecialchars(strip_tags($date)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':grup', htmlspecialchars(strip_tags($grup)),PDO::PARAM_STR);
    
        if($insert_stmt->execute()){
            $msg['message'] = 'Data Inserted Successfully';
            $msg['data'] = [
                'user_id' => $user_id,
                'tipe_id' => $tipe_id,
                'alamat' => $alamat,
                'kecamatan' => $kecamatan,
                'kelurahan' => $kelurahan,
                'rt' => $rt,
                'rw' => $rw,
                'telepon' => $telepon,
                'patungan_qurban' => $patungan_qurban,
                'penagihan_patungan_qurban' => $penagihan_patungan_qurban,
                'jumlah_penagihan_qurban' => $jumlah_penagihan_qurban,
                'nasab' => $nasab,
                'tanggal' => $date,
                'grup' => $grup
            ];
        }
        else{
            $msg['message'] = 'Data not Inserted';
        } 
        
    }else{
        $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>
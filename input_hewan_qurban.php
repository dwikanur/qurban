<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();
date_default_timezone_set ("Asia/Jakarta");

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg =[];

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($_POST['name']) && isset($_POST['user_id']) && isset($_POST['master_mosque_id']) && isset($_POST['tipe_id']) && isset($_POST['price']) && isset($_POST['weight']) && isset($_POST['photo']) && isset($_POST['description'])){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($_POST['name']) && !empty($_POST['user_id']) && !empty($_POST['master_mosque_id']) && !empty($_POST['tipe_id']) && !empty($_POST['price']) && !empty($_POST['weight']) && !empty($_POST['photo']) && !empty($_POST['description'])){

        $name = $_POST['name'];
        $user_id = $_POST['user_id'];
        $master_mosque_id = $_POST['master_mosque_id'];
        $price = $_POST['price'];
        $weight = $_POST['weight'];

        $photo = $_POST['photo'];
        $fileName = date('YmdHis') . '.png';
        $filePath = 'images/' .$fileName;
        file_put_contents($filePath, base64_decode($photo));

        $tipe_id = $_POST['tipe_id'];
        $description = $_POST['description'];

        $date = date('Y-m-d');
        $status_hewan = 0;

        $insert_query = "INSERT INTO hewan_qurban (name,user_id,master_mosque_id,tipe_id,price,weight,photo
        ,description,date, status_hewan) VALUES (:name,:user_id,:master_mosque_id,:tipe_id,:price,:weight,:photo
        ,:description,:date,:status_hewan)";
        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':name', htmlspecialchars(strip_tags($name)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':user_id', htmlspecialchars(strip_tags($user_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':master_mosque_id', htmlspecialchars(strip_tags($master_mosque_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':price', htmlspecialchars(strip_tags($price)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':weight', htmlspecialchars(strip_tags($weight)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':photo', htmlspecialchars(strip_tags($filePath)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':tipe_id', htmlspecialchars(strip_tags($tipe_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':description', htmlspecialchars(strip_tags($description)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':date', htmlspecialchars(strip_tags($date)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':status_hewan', htmlspecialchars(strip_tags($status_hewan)),PDO::PARAM_STR);
    
        if($insert_stmt->execute()){
            $msg['message'] = 'Data Inserted Successfully';
            $msg['data'] = [
                'name' => $name,
                'user_id' => $user_id,
                'tipe_id' => $tipe_id,
                'status_hewan' => $status_hewan,
                'master_mosque_id' => $master_mosque_id,
                'price' => $price,
                'weight' => $weight,
                'description' => $description,
                'date' => $date,
                'photo' => $photo

            ];
        }
        else{
            $msg['message'] = 'Data not Inserted';
        } 
        
    }else{
        $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>
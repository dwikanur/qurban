<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// $data = json_decode(file_get_contents("php://input"));

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
if(isset($_POST['tipe_id'])){
    $tipe_id = $_POST['tipe_id'];

    $get_pendaftaran = "SELECT  users.email,users.telephone_number,users.username,users.no_kk ,
        SUM(tabungan_user.nominal) total_tabungan,pendaftaran.id, pendaftaran.user_id, pendaftaran.tipe_id, pendaftaran.alamat, pendaftaran.kecamatan, pendaftaran.kelurahan, pendaftaran.rt, pendaftaran.rw, pendaftaran.telepon, pendaftaran.patungan_qurban, pendaftaran.penagihan_patungan_qurban, pendaftaran.jumlah_penagihan_qurban, pendaftaran.nasab, pendaftaran.date, pendaftaran.grup
FROM    users
        LEFT JOIN tabungan_user
            ON users.id = tabungan_user.user_id
        LEFT JOIN pendaftaran
            ON users.id = pendaftaran.user_id
            where pendaftaran.tipe_id = 1
GROUP BY users.email,users.telephone_number,users.username,users.no_kk,pendaftaran.id, pendaftaran.user_id, pendaftaran.tipe_id, pendaftaran.alamat, pendaftaran.kecamatan, pendaftaran.kelurahan, pendaftaran.rt, pendaftaran.rw, pendaftaran.telepon, pendaftaran.patungan_qurban, pendaftaran.penagihan_patungan_qurban, pendaftaran.jumlah_penagihan_qurban, pendaftaran.nasab, pendaftaran.date, pendaftaran.grup
ORDER BY pendaftaran.date ASC";
    $get_stmt = $conn->prepare($get_pendaftaran);
    $get_stmt->execute();

    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($get_stmt->rowCount() > 0){
        
        // CREATE POSTS ARRAY
    $array = [];
        
        while($row = $get_stmt->fetch(PDO::FETCH_ASSOC)){
            
            $data = [
                'id' => $row['id'],
                'user' => [
                    'user_id' => $row['user_id'],
                    'email' => $row['email'],
                    'telephone_number' => $row['telephone_number'],
                    'username' => $row['username'],
                    'no_kk' => $row['no_kk']
                ],
                'tipe_id' =>$row['tipe_id'],
                'alamat' => $row['alamat'],
                'kecamatan' => $row['kecamatan'],
                'kelurahan' => $row['kelurahan'],
                'rt' => $row['rt'],
                'rw' => $row['rw'],
                'telepon' => $row['telepon'],
                'patungan_qurban' =>$row['patungan_qurban'],
                'penagihan_patungan_qurban' => $row['penagihan_patungan_qurban'],
                'jumlah_penagihan_qurban' => $row['jumlah_penagihan_qurban'],
                'total_tabungan' => $row['total_tabungan'],
                'nasab' => $row['nasab'],
                'date' => $row['date'],
                'grup' => $row['grup']
            ];
            // PUSH POST DATA IN OUR $posts_array ARRAY
            array_push($array, $data);
        }
        $response['data'] = $array;
        //SHOW POST/POSTS IN JSON FORMAT
        echo json_encode($response);

    }
    else{
        //IF THER IS NO POST IN OUR DATABASE
        echo json_encode(['message'=>'Data tidak tersedia']);
    }
}else{
    echo json_encode(['message'=>'tipe tidak tersedia']);
}
?>